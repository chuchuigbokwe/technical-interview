#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Created by Chukwunyere Igbokwe on January 26, 2017 by 12:55 PM

import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import matplotlib as mpl
from math import *

class KalmanFilter:
    def __init__(self, A,H,x,P,Q,R):
        self.A = A
        self.H = H
        self.x = x
        self.P = P
        self.Q = Q
        self.R = R
        self.I = np.eye()


    def _update(self):
        pass

    def _predict(self):
        pass
    def Gaussian(self, mu, sigma, x):
        '''

        :param mu:
        :param sigma:
        :param x:
        :return:
        '''
        # calculates the probability of x for 1-dim Gaussian with mean mu and var. sigma
        return exp(- ((mu - x) ** 2) / (sigma ** 2) / 2.0) / sqrt(2.0 * pi * (sigma ** 2))

    def Gausian_predict(pos, variance, movement, movement_variance):
        return (pos + movement, variance + movement_variance)

    def Gaussian_update(mu1, var1, mu2, var2):
        mean = (var1 * mu2 + var2 * mu1) / (var1 + var2)
        variance = 1 / (1 / var1 + 1 / var2)
        return (mean, variance)

